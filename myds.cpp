/*
           design of a data structures having
           insertion in O(1)
           remove in O(1)
           search in O(1)
           find random element in O(1)
  
*/

#include <bits/stdc++.h>
using namespace std;

      class Myds
              {
             vector< int > v;     //  stores by index 
             map< int , int>m;     // map that stores value as key and index as value 
             public:
                    void add( int x )
                    {
                          if( m.find( x) != m.end() )  // if already present
                                return ;
                          int i = v.size();
                          v.push_back( x );
                          m[ x ]= i;
                    }
                    void remove( int x )
                    {
                        if( m.find(x)== m.end() )
                              return ;
                        int i= m[x];
                        m.erase( x );
                        int j= v.size() -1;
                        swap( v[i],v[j] );
                        m[ v[i]] = i;
                        v.pop_back();
                    }
                    int search( int x )
                     {
                         if( m.find( x) != m.end() )
                               return m[x];
                         return -1;
                     }
                    int getrandom( )
                    {
                        srand( time(NULL));
                        int i= rand( ) % v.size() ;
                        return v[i];
                    }
              };
int main()
{   Myds  ds;
    ds.add( 2 );
    ds.add(6 );
    ds.add( 10 );
    cout<< ds.getrandom() <<endl;
    cout<< ds.getrandom() <<endl;
    ds.add( 23 );
    ds.add( 78 );
    ds.add( 2334 );
    ds.add( 122233 );
    cout<< ds.getrandom() <<endl;
    cout<< ds.getrandom() <<endl;
     ds.remove( 78 );
      cout<< ds.getrandom() <<endl;
      cout<< ds.search( 78 )<<endl;

    return 0;
}
